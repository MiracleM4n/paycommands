package com.miraclem4n.paycommands;

import org.bukkit.block.CreatureSpawner;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public class CommandHandler implements CommandExecutor {

    PayCommands plugin;

    public CommandHandler(PayCommands plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String cmd = command.getName();

        if (!cmd.equalsIgnoreCase("paycommands"))
            return false;

        if (args.length == 0)
            return false;

        if (args[0].equalsIgnoreCase("reload")
                || args[0].equalsIgnoreCase("r")) {
            if (!getAPI().hasPerm(sender, "paycommands.reload")) {
                sender.sendMessage("You don't have permission to do this.");
                return true;
            }

            plugin.setupConfigs();
            sender.sendMessage(formatMessage("Config Reloaded."));

            return true;
        }

        if (!(sender instanceof  Player)) {
            sender.sendMessage(formatMessage("Console's cannot buy MobSpanwers."));
            return false;
        }

        Player player = (Player) sender;

        if (args[0].equalsIgnoreCase("sell")) {
            try {
                CreatureSpawner spawner = getAPI().getSpawner(player);

                if (spawner == null) {
                    sender.sendMessage(formatMessage("Spawner not found/Too far away."));
                    return true;
                }

                Double cost = getAPI().getSaleCost(spawner);

                if (!getAPI().isChangeable(player, spawner, cost, "paycommands.sell")) {
                    sender.sendMessage(formatMessage("You cannot sell this Spawner."));
                    return true;
                }

                spawner.setCreatureTypeByName(EntityType.PIG.getName());
                spawner.setDelay(20);

                getAPI().setOwnership(null, spawner, cost);
                getAPI().giveMoney(player.getName(), cost);

                sender.sendMessage(formatMessage("Owner of this MobSpawner has been reset."));
                sender.sendMessage(formatMessage(cost + " has been added from your account."));
                return true;
            } catch (Exception ex) {
                ex.printStackTrace();
                sender.sendMessage(formatMessage("Error resetting MobSpawner Type."));

                return true;
            }
        }

        if (!(args.length > 1))
            return false;

        if (args[0].equalsIgnoreCase("buy")) {
            try {
                CreatureSpawner spawner = getAPI().getSpawner(player);

                if (spawner == null) {
                    sender.sendMessage(formatMessage("Spawner not found/Too far away."));
                    return true;
                }

                MobData mobData = new MobData(args[1], false);

                if (mobData.getEntityName() == null) {
                    sender.sendMessage(formatMessage("Mob not found."));
                    return true;
                }

                if (!mobData.isEnabled()) {
                    sender.sendMessage(formatMessage("Mob isn't enabled."));
                    return true;
                }

                Double cost = mobData.getPurchaseCost();

                if (!getAPI().hasMoney(player.getName(), cost)) {
                    sender.sendMessage(formatMessage("You don't have enough money to enable this mob."));
                    return true;
                }

                if (!getAPI().isChangeable(player, spawner, mobData.getSaleCost(), "paycommands.buy")) {
                    sender.sendMessage(formatMessage("You cannot change the type of this Spawner."));
                    return true;
                }

                spawner.setCreatureTypeByName(mobData.getEntityName());
                spawner.setDelay(mobData.getSpawnRate());

                getAPI().setOwnership(player.getName(), spawner, mobData.getSaleCost());
                getAPI().removeMoney(player.getName(), cost);

                sender.sendMessage(formatMessage("MobSpawner Type set to: " + mobData.getEntityName() + "."));
                sender.sendMessage(formatMessage("You are now the owner of this MobSpawner."));
                sender.sendMessage(formatMessage(cost + " has been removed from your account."));

                return true;
            } catch (Exception ex) {
                ex.printStackTrace();
                sender.sendMessage(formatMessage("Error setting MobSpawner Type."));
                return true;
            }
        }

        return false;
    }

    String formatMessage(String message) {
        return (getAPI().addColour("&4[" + (plugin.pdfFile.getName()) + "] " + message));
    }

    public API getAPI() {
        return new API(plugin);
    }
}
