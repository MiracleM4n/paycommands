package com.miraclem4n.paycommands;

import org.bukkit.configuration.file.YamlConfiguration;

@SuppressWarnings("unused")
public class ConfigListener {
    PayCommands plugin;
    Boolean hasChanged = false;
    YamlConfiguration config;

    public ConfigListener(PayCommands plugin) {
        this.plugin = plugin;

        load();

        config = plugin.config;

        config.options().indent(4);
        config.options().header("PayCommands Configuration File");
    }

    void load() {
        plugin.config = YamlConfiguration.loadConfiguration(plugin.configF);
    }

    void save() {
        try {
            plugin.config.save(plugin.configF);
        } catch (Exception ignored) { }
    }

    void checkConfig() {
        for (DefaultMobData mob : DefaultMobData.values()) {
            editOption(config, mob.name() + ".cost", mob.name() + ".cost.buy");

            checkOption(config, mob.name() + ".cost.buy", mob.getDefaultPurchaseCost());
            checkOption(config, mob.name() + ".cost.sell", mob.getDefaultSaleCost());
            checkOption(config, mob.name() + ".entityName", mob.getDefaultName());
            checkOption(config, mob.name() + ".enabled", mob.isEnabledByDefault());
            checkOption(config, mob.name() + ".names", mob.getDefaultNickNames());
            checkOption(config, mob.name() + ".spawnRate", mob.getDefaultSpawnRate());
        }

        if (hasChanged)
            save();
    }

    void checkOption(YamlConfiguration config, String option, Object defValue) {
        if (!config.isSet(option)) {
            config.set(option, defValue);

            hasChanged = true;
        }
    }

    void editOption(YamlConfiguration config, String oldOption, String newOption) {
        if (config.isSet(oldOption)) {
            config.set(newOption, config.get(oldOption));
            config.set(oldOption, null);

            hasChanged = true;
        }
    }

    void removeOption(YamlConfiguration config, String option) {
        if (config.isSet(option)) {
            config.set(option, null);

            hasChanged = true;
        }
    }

    private enum DefaultMobData {
        CREEPER("Creeper", 15000.0, 8000.0, true,
                "creep,creeper", 20),

        SKELETON("Skeleton", 15000.0, 8000.0, true,
                "skeleton,skele", 20),

        SPIDER("Spider", 15000.0, 8000.0, true,
                "spider", 20),

        GIANT("Giant", 15000.0, 8000.0, false,
                "giant", 20),

        ZOMBIE("Zombie", 15000.0, 8000.0, true,
                "zombie", 20),

        SLIME("Slime", 15000.0, 8000.0, true,
                "slime", 20),

        GHAST("Ghast", 15000.0, 8000.0, false,
                "ghast", 20),

        PIG_ZOMBIE("PigZombie", 15000.0, 8000.0, true,
                "zombie_pigman,zombie_pig,zombiepigman,pigzombie,pig_zombie", 20),

        ENDERMAN("Enderman", 15000.0, 8000.0, true,
                "enderman", 20),

        CAVE_SPIDER("CaveSpider", 15000.0, 8000.0, true,
                "cave_spider,cavespider", 20),

        SILVERFISH("Silverfish", 15000.0, 8000.0, true,
                "silverfish,silver_fish", 20),

        BLAZE("Blaze", 15000.0, 8000.0, true,
                "blaze", 20),

        MAGMA_CUBE("LavaSlime", 15000.0, 8000.0, true,
                "magma_cube,magmacube,lavaslime,lava_slime", 20),

        ENDER_DRAGON("Enderdragon", 15000.0, 8000.0, false,
                "enderdragon,dragon", 20),

        PIG("Pig", 5000.0, 3000.0, true,
                "pig", 20),

        SHEEP("Sheep", 5000.0, 3000.0, true,
                "sheep", 20),

        COW("Cow", 5000.0, 3000.0, true,
                "cow", 20),

        CHICKEN("Chicken", 5000.0, 3000.0, true,
                "chicken,chick", 20),

        SQUID("Squid", 5000.0, 3000.0, true,
                "squid", 20),

        WOLF("Wolf", 5000.0, 3000.0, true,
                "wolf", 20),

        MUSHROOM_COW("MushroomCow", 5000.0, 3000.0, true,
                "mooshroom,moosh,mushroomcow,mushroom_cow", 20),

        SNOWMAN("SnowMan", 5000.0, 3000.0, true,
                "snowgolem,snow_golem,snowman,snow_man", 20),

        OCELOT("Ozelot", 5000.0, 3000.0, true,
                "ocelot,ozelot", 20),

        IRON_GOLEM("VillagerGolem", 15000.0, 8000.0, true,
                "irongolen,iron_golem", 20),

        VILLAGER("Villager", 5000.0, 3000.0, true,
                "villager", 20);

        private final String cValue;
        private final Double pCost;
        private final Double sCost;
        private final Boolean enabled;
        private final String names;
        private final Integer spawnRate;

        DefaultMobData(String cValue, Double pCost, Double sCost, Boolean enabled, String names, Integer spawnRate) {
            this.cValue = cValue;
            this.pCost = pCost;
            this.sCost = sCost;
            this.enabled = enabled;
            this.names = names;
            this.spawnRate = spawnRate;
        }

        public String getDefaultName() {
            return cValue;
        }

        public Boolean isEnabledByDefault() {
            return enabled;
        }

        public Double getDefaultPurchaseCost() {
            return pCost;
        }

        public Double getDefaultSaleCost() {
            return sCost;
        }

        public String[] getDefaultNickNames() {
            return names.split(",");
        }

        public Integer getDefaultSpawnRate() {
            return spawnRate;
        }
    }
}
