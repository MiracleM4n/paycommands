package com.miraclem4n.paycommands;

import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class MobData {
    YamlConfiguration config;

    String name;
    String entityName;

    Double pCost;
    Double sCost;

    Boolean enabled;

    Integer spawnRate;

    public MobData(String namez, Boolean exactMatch) {
        config = YamlConfiguration.loadConfiguration(new File("plugins/PayCommands/config.yml"));

        if (exactMatch)
            this.name = namez;
        else
            name = getMob(namez);

        entityName = config.getString(name + ".entityName", "Pig");

        pCost = config.getDouble(name + ".cost.buy", 5000.0);
        sCost = config.getDouble(name + ".cost.sell", 2000.0);

        enabled = config.getBoolean(name + ".enabled", true);

        spawnRate = config.getInt(name + ".spawnRate", 20);
    }

    public String getName() {
        return name;
    }

    public String getEntityName() {
        return entityName;
    }

    public Double getPurchaseCost() {
        return pCost;
    }

    public Double getSaleCost() {
        return sCost;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public Integer getSpawnRate() {
        return spawnRate;
    }

    String getMob(String arg) {
        for (String mob : config.getKeys(false))
            for (String names : config.getStringList(mob + ".names"))
                if (names.equalsIgnoreCase(arg))
                    return mob;

        for (String mob : config.getKeys(false))
            for (String names : config.getStringList(mob + ".names"))
                if (names.contains(arg))
                    return mob;

        return "PIG";
    }
}
