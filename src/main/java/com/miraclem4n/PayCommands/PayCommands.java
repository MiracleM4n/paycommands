package com.miraclem4n.paycommands;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.Date;

public class PayCommands extends JavaPlugin {
    // Default Plugin Data
    PluginManager pm;
    PluginDescriptionFile pdfFile;

    // Configuration
    YamlConfiguration config = null;

    // Configuration Files
    File configF = null;

    // Vault
    Boolean vaultB = false;
    Permission perm = null;
    Economy econ = null;

    // Booleans
    Boolean useEcon = true;

    // Timers
    long sTime1;
    long sTime2;
    long sDiff;

    public void onEnable() {
        // 1st Startup Timer
        sTime1 = new Date().getTime();

        // Initialize Plugin Data
        pm = getServer().getPluginManager();
        pdfFile = getDescription();

        // Initialize Configs
        configF = new File(getDataFolder(), "config.yml");
        config = YamlConfiguration.loadConfiguration(configF);

        // Manage Config options
        config.options().indent(4);

        // Setup Configs
        setupConfigs();

        // Setup Vault
        setupVault();

        // Register Commands
        if (!useEcon || vaultB)
            getCommand("paycommands").setExecutor(new CommandHandler(this));

        // 2nd Startup Timer
        sTime2 = new Date().getTime();

        // Calculate Startup Timer
        sDiff = (sTime2 - sTime1);

        if (!useEcon || vaultB)
            getAPI().log("[" + pdfFile.getName() + "] " + pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled! [" + sDiff + "ms]");
        else
            pm.disablePlugin(this);
    }

    public void onDisable() {
        getAPI().log("[" + pdfFile.getName() + "] " + pdfFile.getName() + " version " + pdfFile.getVersion() + " is disabled!");
    }

    void setupVault() {
        Plugin plugin = pm.getPlugin("Vault");

        if (pm.getPlugin("Vault") == null)
            return;

        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(Permission.class);
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(Economy.class);

        if (permissionProvider != null)
            perm = permissionProvider.getProvider();
        else
            getAPI().log("[" + pdfFile.getName() + "] " + plugin.getDescription().getName() + " Vault Perms not found disabling.");

        if (economyProvider != null)
            econ = economyProvider.getProvider();
        else if (useEcon)
            getAPI().log("[" + pdfFile.getName() + "] " + plugin.getDescription().getName() + " Vault Econ not found disabling.");

        vaultB = econ != null
                && perm != null;

        if (vaultB)
            getAPI().log("[" + pdfFile.getName() + "] " + plugin.getDescription().getName() + " " + (plugin.getDescription().getVersion()) + " found hooking in.");
    }

    void setupConfigs() {
        new ConfigListener(this).checkConfig();
    }

    public API getAPI() {
        return new API(this);
    }
}
